package io.muic.ooc.homework1.problem4.Test;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by panteparak on 1/20/17.
 */
public class URITEST {
    public static void main(String[] args) {
        try {
            URI test = new URI("../../css/css.hd");
            System.out.println(test.getPath().replace("../", ""));
        } catch (URISyntaxException e){
            e.printStackTrace();
        }

    }
}
