package io.muic.ooc.homework1.problem4;

import org.apache.commons.io.FilenameUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by panteparak on 1/19/17.
 */
public class WebCrawler {
    private final Set<String> webpageSet;
    private final Set<String> fileSet;
    private final String downloadURL;
    private final File saveLocation;
    private final Downloader downloader;
    private URI downloadURI = null;

    public WebCrawler(String urlToCrawl, File saveDirectory) {
        this.downloadURL = urlToCrawl;
        try {
            this.downloadURI = new URI(urlToCrawl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.saveLocation = saveDirectory;
        this.downloader = new Downloader(saveDirectory, urlToCrawl);
        this.webpageSet = new LinkedHashSet<>();
        this.fileSet = new LinkedHashSet<>();
    }

    public void crawl(){
        final HashSet<String> toBeDownloadURL = new HashSet<>(Arrays.asList(this.downloadURL));
        while (!toBeDownloadURL.isEmpty()){
            System.out.println("Size: " + toBeDownloadURL.size());
            System.out.println(toBeDownloadURL);
            System.out.println();

            Set<String> parsedURL = new LinkedHashSet<>();

            toBeDownloadURL.forEach((url) -> {
                File htmlFile = addURL(url);
                if (htmlFile != null){
                    parsedURL.addAll(htmlParser(htmlFile));
                }
            });

            toBeDownloadURL.clear();
            toBeDownloadURL.addAll(parsedURL);
        }
    }

    private boolean isValidURL(String url){
        URI uri;
        try {
            uri = new URI(url.trim());
            if (uri.getHost() == null) return true;
            if (!uri.getHost().trim().equals(this.downloadURI.getHost().trim())) return false;
        } catch (URISyntaxException e){
            e.printStackTrace();
        }

        return true;
    }

    private String urlCleanUp(String url){
        if (!url.contains(this.downloadURI.getHost())) {
            try {
                url = url.replace("../", "");

                url = new URL(new URL(this.downloadURL), url).toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        if (url.contains("#")) url = url.substring(0, url.indexOf("#"));
        if (url.contains("?")) url = url.substring(0, url.indexOf("?"));
        return url.trim();
    }

    private String getAbsoluteURL(String url){
        return url.endsWith("/") ? url + "index.html" : url;
    }

    private File addURL(String url){
        if (isValidURL(url)){
            String out = urlCleanUp(url);
            out = getAbsoluteURL(out);
            if (out.endsWith(".html")) this.webpageSet.add(out);
            else this.fileSet.add(url);
            return downloader.file(out);
        }

        return null;
    }

    private Set<String> htmlParser(File htmlFilePath){
        Set<String> tempSet = new LinkedHashSet<>();
        try {

            String[] keys = {"src", "href"};
            Document document = Jsoup.parse(htmlFilePath, "UTF-8");
            for (String key : keys){
                document.body().getElementsByAttribute(key).forEach((item) -> {
                    if (item.attr(key) != null) tempSet.add(item.attr(key));
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return tempSet;
    }

    public static void main(String[] args) {
        String url = "https://cs.muic.mahidol.ac.th/courses/ooc/docs/";
        File saveDir = new File("/Users/panteparak/");
        WebCrawler crawler = new WebCrawler(url, saveDir);

        System.out.println(crawler.urlCleanUp("https://cs.muic.mahidol.ac.th/courses/ooc/docs/"));
        System.out.println(crawler.urlCleanUp("https://cs.muic.mahidol.ac.th/courses/ooc/docs/de"));
        System.out.println(crawler.urlCleanUp("https://cs.muic.mahidol.ac.th/courses/ooc/docs/ACD.D"));
        System.out.println(crawler.urlCleanUp("/courses/ooc/docs/"));

    }
}
