package io.muic.ooc.homework1.problem4;

import java.io.File;

/**
 * Created by panteparak on 1/19/17.
 */
public class main {
    public static void main(String[] args) {
        String url = "https://cs.muic.mahidol.ac.th/courses/ooc/docs/";
        WebCrawler webCrawler = new WebCrawler(url, new File("/Users/panteparak/Downloads/TestRun/"));
        webCrawler.crawl();
    }
}
