package io.muic.ooc.homework1.problem4;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by panteparak on 1/19/17.
 */
public class Downloader {

    static class config{
        public static final int TIMEOUT_MS = 10000;
        public static final int MAX_CONNECTION = 200;
        public static final int MAX_CONNECTION_PER_ROUTE = 200;
//        public static final long MAX_KEEP_ALIVE = TimeUnit.SECONDS.toMillis(); // Time
        public static final int BUFFER_SIZE = 1024 * 32;
    }

    private File saveLocation;
    private CloseableHttpClient connection;
    private String rootURL;

    public Downloader(File saveFolder, String rootURL) {
        this.saveLocation = saveFolder;
        this.connection = connectionInit();
        this.rootURL = rootURL;

        System.out.println(this.rootURL);
    }

    private CloseableHttpClient connectionInit(){
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(config.MAX_CONNECTION);
        connectionManager.closeExpiredConnections();
        connectionManager.setDefaultMaxPerRoute(config.MAX_CONNECTION_PER_ROUTE);

        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(config.TIMEOUT_MS)
                .setConnectTimeout(config.TIMEOUT_MS)
                .setConnectionRequestTimeout(config.TIMEOUT_MS)
                .setRedirectsEnabled(true)
                .build();

        CloseableHttpClient httpClient = HttpClients
                .custom()
                .setDefaultRequestConfig(requestConfig)
                .setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())
                .setConnectionReuseStrategy((HttpResponse res, HttpContext context) -> true)
                .setConnectionManager(connectionManager)
                .build();

        return httpClient;
    }

    public File file(String url){
//        Work Around
        if (!url.contains(this.rootURL)) {
            System.out.println("BLAHHHHH");
            return null;
        }
        File absoluteSavePath = generateSaveLocation(url);
        System.out.println(url);

        try {

            HttpGet httpGet = new HttpGet(url);
            HttpResponse res = this.connection.execute(httpGet);
            HttpEntity httpEntity = res.getEntity();

            if (httpEntity != null) {
                InputStream is = httpEntity.getContent();
                FileOutputStream fos = new FileOutputStream(absoluteSavePath);

                byte[] maxBuffer = new byte[config.BUFFER_SIZE];
                int byteCount;
                while ((byteCount = is.read(maxBuffer)) > 0) {
                    fos.write(maxBuffer, 0, byteCount);
                }
                fos.close();
                is.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        return absoluteSavePath;
    }

    private void createDirectory(File saveDirectory){
        if (!saveDirectory.exists()) saveDirectory.mkdirs();
    }

    private File generateSaveLocation(String downloadURL){
        Path saveDir = Paths.get(this.saveLocation.toString(), "docs", downloadURL.replace(this.rootURL, ""));
        createDirectory(new File(saveDir.toFile().getParent()));
        return saveDir.toFile();

    }

    public static void main(String[] args) {
//        File s = Paths.get("/","Users", "panteparak", "Downloads", "TestRun").toFile();
//        Downloader downloader = new Downloader(s, "https://cs.muic.mahidol.ac.th/courses/ooc/docs/");
//        System.out.println(downloader.generateSaveLocation("https://cs.muic.mahidol.ac.th/courses/ooc/docs/cbc"));
    }
}
