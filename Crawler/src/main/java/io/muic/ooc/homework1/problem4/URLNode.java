package io.muic.ooc.homework1.problem4;

public class URLNode {
    private String rootURL;
    private String pageLink;
    private boolean isDownloaded;

    public URLNode(String rootURL, String pageLink) {
        this.rootURL = rootURL;
        this.pageLink = pageLink;
        this.isDownloaded = false;
    }

    @Override
    public String toString() {
        return "URLNode{" +
                "rootURL='" + rootURL + '\'' +
                ", pageLink='" + pageLink + '\'' +
                '}';
    }

    public String getCompiledURL(){
        return "";
    }

    public void toggleDownload(){
        this.isDownloaded = true;
    }

    public boolean isDownloaded(){
        return this.isDownloaded;

    }
}
