package io.muic.ooc.homework1.problem4.Test;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by panteparak on 1/20/17.
 */
public class hs{
    public static void main(String[] args) {
        HashSet<Integer> strings = new LinkedHashSet<>();
        strings.add(5);
        strings.add(2);
        strings.add(5);

//        System.out.println();

        strings.forEach((action) -> {
            System.out.println(action);
        });

        System.out.println();

        strings.iterator().forEachRemaining((action) -> {
            System.out.println(action);
        });
    }
}
